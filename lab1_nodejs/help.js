function stringToArr(str) {
  var temp = str.split(/\n|\r| /);
  temp = temp.filter(Boolean);
  return temp.map(Number);
}
function sumArray(data) {
  data.reduce((a, b) => a + b, 0);
}
function factorial(n) {
  return n != 1 ? n * factorial(n - 1) : 1;
}

function makeAnsw(data, n) {
  var alp = ["a", "b", "c", "d", "e", "f", "g", , "h", "i", "j", "k", "l", "m"];
  var number = data.shift();
  data = data.shift();
  var ans = [];
  for (var i = 0; i < n; i++) {
    ans.push(alp[data[i]]);
  }
  return `${ans}\n${number}`;
}

function calculate(data, N) {
  var best = 0;
  var Order = [];

  for (var k = 0; k < N; k++) {
    var temp = 0;
    var tempOrder = [];
    for (var i = 0+k; i < data.length; i += N + 1) {
      temp += data[k + i];
      tempOrder.push(i - tempOrder.length * N);
    }
    if (temp > best) {
      best = temp;
      Order = tempOrder;
    }
  }
  for (var k = 0; k < N - 1; k++) {
    var temp = 0;
    var tempOrder = [];
    for (var i = 0+k; i < data.length; i += N + 1) {
      temp += data[N + k + i];
      tempOrder.push(i - tempOrder.length * N);
    }
    if (temp > best) {
      best = temp;
      Order = tempOrder;
    }
  }
  return [data.reduce((a, b) => a + b, 0) - best, Order];
}

function min(N, sum) {
  var alphabet = ["a", "b", "c", "d", "e", "f", "g", "h"];
}

module.exports = { stringToArr, factorial, calculate, min, sumArray, makeAnsw };
