const fs = require("fs");
const help = require("./help");

var data = fs.readFileSync("input.txt", "utf8");
var result = "NULL";
data = help.stringToArr(data);
var N = data.shift();

var answer = help.makeAnsw(help.calculate(data, N), N);

fs.writeFileSync("output.txt", `${answer}`);
console.log(answer);
