const fs = require("fs");
const help = require("./help");
var answer = 0;
var data = fs.readFileSync("input.txt", "utf8");

var data = help.stringToArr(data);
console.log(data);
answer = help.calculate(data);
fs.writeFileSync("output.txt", `${answer}`);
